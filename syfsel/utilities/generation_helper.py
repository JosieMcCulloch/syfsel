"""This module contains functions to help generate fuzzy sets."""

import numpy as np
from decimal import Decimal


from .gaussian import Gaussian
from . import general_helper as gelh


np.random.seed(1000)  # use fixed seed so results can be replicated


def generate_locs_and_scales(uod, var, samples):
    """Return randomly generated means and std devs.

    uod: Range of x values locs fall between
    samples: Total locs and scaled generated
    Returns two lists [locs] [scales]"""
    def _rescale(c, rng):
        return round(c * (rng[1] - rng[0]) + rng[0], 2)
    locs = [_rescale(x, uod) for x in np.random.random(samples)]
    # Make scale no smaller than 0.025 to avoid issues rounding to 1dp
    scales = [max(0.025, _rescale(x, var)) for x in np.random.random(samples)]
    return locs, scales


def generate_weights(samples, weight_range):
    """Generage random floats between within weight_range

    samples: total weights generated
    weight_range: length 2 list e.g. [0.4, 0.6]"""
    return [gelh.set_x(np.random.uniform(weight_range[0], weight_range[1]))
            for i in range(samples)]


def calculate_zlevel_coords(n=4):
    """Calculate the zlevel coordinates for each zslice."""
    # it's easier to construct the z-coordinates in reverse order
    zlevel_coords = [gelh.set_x(Decimal(i)/n) for i in range(n, 0, -1)]
    zlevel_coords.reverse()
    return zlevel_coords


def generate_zslices(lmf, umf, n=4):
    """Generate the zslices fuzzy sets.

    lmf: lower membership function
    umf: upper membership function
    n: total zlevels (default:4)"""
    zlevel_coords = calculate_zlevel_coords(n)
    zslice_functions = {zlevel_coords[0]: (lmf, umf)}
    l_args = [lmf.mean, lmf.std_dev, lmf.height]
    u_args = [umf.mean, umf.std_dev, umf.height]
    # Calculate how much the MFs shift for each zslice.
    # coord_skew defines how much the coordinates of the UMF and
    # LMF change at each zSlice; 1 means no skew and 0 means
    # maximum skew. Think of it as a percentage of how far apart
    # the UMF and LMF will be from each other between each zLevel.
    if n == 1:
        coord_skew = [1]
    else:
        x = n - 1
        coord_skew = [Decimal(i)/x for i in range(x, -1, -1)]
    # Create the zslices IT2 FSs for each zlevel.
    # start from index 1 because lmf and umf already define the lowest zslice
    for z_index in range(1, len(coord_skew)):
        lmf_args = []
        umf_args = []
        for i in range(len(l_args)):
            spread = (u_args[i] - l_args[i]) / Decimal(2)
            altered_value = gelh.set_x(spread * (1 - coord_skew[z_index]))
            lmf_args.append(l_args[i] + altered_value)
            umf_args.append(u_args[i] - altered_value)
        zslice = (Gaussian(lmf_args[0], lmf_args[1], lmf_args[2]),
                  Gaussian(umf_args[0], umf_args[1], umf_args[2]))
        zslice_functions[zlevel_coords[z_index]] = zslice
    return zslice_functions
 
