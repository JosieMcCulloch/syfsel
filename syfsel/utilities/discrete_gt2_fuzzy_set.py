"""This module is used to create a discrete type-2 fuzzy set."""

from decimal import Decimal
from collections import defaultdict

from utilities import general_helper as gelh


class DiscreteGT2FuzzySet():
    """Create a discrete type-2 fuzzy set."""

    def __init__(self, points, uod=(0, 1)):
        """Create a discrete GT2 fuzzy set as a dict as {x: {z: (lmf, umf}}."""
        self.points = points
        self.uod = uod
        self.zlevel_coords = []
        map(self.zlevel_coords.extend, [d.keys() for d in self.points.values()])
        self.zlevel_coords = sorted(list(set(self.zlevel_coords)))

    def calculate_membership(self, x, z):
        """Calculate the membership of x within the uod.

        Returns a Decimal value.
        """
        try:
            return (gelh.set_x(self.points[x][z][0]),
                    gelh.set_x(self.points[x][z][1]))
        except KeyError:
            return 0, 0

    def calculate_secondary_membership(self, x, mu):
        """Calculate the secondary membership of x at primary membership y."""
        for z in sorted(self.zlevel_coords, reverse=True):
            l, u = self.calculate_membership(x, z)
            if l < mu < u:
                return z
        return 0
