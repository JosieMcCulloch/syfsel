"""This module contains general functions used by other helper modules."""

from itertools import chain
from decimal import Decimal


def validate_subsetrange(subsets):
    """Convert sys arguments of subset range from str to int range."""
    if subsets == '-':
        return None
    subsets = subsets.split('.')
    linkchain = []
    for subset in subsets:
        if '-' not in subset:
            linkchain.append((int(subset),))
        else:
            subsetrange = subset.split('-')
            if subsetrange[0] == '' and subsetrange[1] == '':
                linkchain.append((None, None))
            else:
                linkchain.append(range(int(subsetrange[0]), int(subsetrange[1])+1))
    return list(chain.from_iterable(i for i in linkchain))


def set_x(x):
    """Set x as Decimal type to 4 decimal places."""
    return Decimal(x).quantize(Decimal(10) ** -4)
 
