"""This module is used to create a discrete type-1 fuzzy set."""

from decimal import Decimal

from utilities import general_helper as gelh


class DiscreteT1FuzzySet():
    """Create a discrete type-1 fuzzy set."""

    def __init__(self, points):
        """Create a discrete type-1 fuzzy set using a dict of x,mu pairs."""
        self.points = points

    def calculate_membership(self, x):
        """Calculate the membership of x within the uod.

        Returns a Decimal value.
        """
        try:
            return gelh.set_x(self.points[x])
        except KeyError:
            return 0
