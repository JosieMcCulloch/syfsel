"""This module is used to save and load fuzzy sets."""

import numpy as np
from collections import defaultdict

from . import generation_helper as genh
from . import general_helper as gelh


def save_t1(sets, filepath, uod, disc):
    """Save the data to the given filepath."""
    fsock = open(filepath, 'wb')
    for x in np.linspace(uod[0], uod[1], disc):
        line = ','.join([str(s.calculate_membership(gelh.set_x(x))) for s in sets])
        fsock.write(str(x) + ',' + line + '\n')
    fsock.close()


def load_t1(filepath):
    """Return list of {x,y} dicts from csv filepath that details fuzzy sets."""
    fsock = open(filepath, 'r')
    firstline = fsock.readline().strip().split(',')
    total_sets = len(firstline) - 1
    sets = [{firstline[0]:y} for y in firstline[1:]]
    for line in fsock:
        values = line.strip().split(',')
        for i in range(1, total_sets+1):
            sets[i-1][values[0]] = values[i]
    fsock.close()
    return sets


def save_it2(sets, filepath, uod, disc):
    """Save the data to the given filepath."""
    def set_tuple(t):
        return str(gelh.set_x(t[0])) + ';' + str(gelh.set_x(t[1]))
    fsock = open(filepath, 'wb')
    for x in np.linspace(uod[0], uod[1], disc):
        line = ','.join([set_tuple(s.calculate_membership(gelh.set_x(x)))
                         for s in sets])
        fsock.write(str(x) + ',' + line + '\n')
    fsock.close()


def load_it2(filepath):
    """Return list of {x:(lmf, umf)} dicts from csv filepath."""
    fsock = open(filepath, 'r')
    firstline = fsock.readline().strip().split(',')
    total_sets = len(firstline) - 1
    sets = [{firstline[0]:y.split(';')} for y in firstline[1:]]
    for line in fsock:
        values = line.strip().split(',')
        for i in range(1, total_sets+1):
            sets[i-1][values[0]] = values[i].split(';')
    fsock.close()
    return sets


def save_gt2(sets, filepath, uod, disc, nzlevels):
    """Save the data to the given filepath."""
    def set_tuple(t):
        return str(gelh.set_x(t[0])) + ';' + str(gelh.set_x(t[1]))
    fsock = open(filepath, 'wb')
    for x in np.linspace(uod[0], uod[1], disc):
        line = str(x)
        for z in genh.calculate_zlevel_coords(nzlevels):
            line += ',' + str(z) + '|'
            line += '|'.join([set_tuple(s.calculate_membership(gelh.set_x(x),
                                                               gelh.set_x(z)))
                              for s in sets])
        fsock.write(line + '\n')
    fsock.close()


def load_gt2(filepath):
    """Return list of {x: {z: (lmf, umf)}} dicts from csv filepath."""
    fsock = open(filepath, 'r')
    total_sets = len(fsock.readline().strip().split(',')[1].split('|'))-1
    total_zlevels = len(fsock.readline().strip().split(','))-1
    sets = [defaultdict(dict) for i in range(total_sets)]
    fsock.seek(0)
    for line in fsock:
        values = line.strip().split(',')
        x = values[0]
        for z in range(1, total_zlevels+1):
            zslices = values[z].split('|')
            z = zslices[0]
            for set_i in range(total_sets):
                sets[set_i][x][z] = zslices[set_i+1].split(';')
    return sets
 
