"""This module is used to create a discrete type-2 fuzzy set."""

from decimal import Decimal
from collections import defaultdict

from utilities import general_helper as gelh


class DiscreteIT2FuzzySet():
    """Create a discrete type-2 fuzzy set."""

    def __init__(self, points):
        """Create a discrete IT2 fuzzy set using a dict as {x: (lmf, umf}."""
        self.points = points

    def calculate_membership(self, x):
        """Calculate the membership of x within the uod.

        Returns a Decimal value.
        """
        try:
            return (gelh.set_x(self.points[x][0]),
                    gelh.set_x(self.points[x][1]))
        except KeyError:
            return 0
