"""Generate fuzzy sets with different types of membership functions.

run:      python generate_sets.py mode samples filepath

example:  python generate_sets.py 1 5 sets.csv

mode:     Indicates the type of fuzzy sets generated
          1: Gaussian functions
          2: Gaussian-based skewed/bimodal functions
          3. Fuzzy logic system outputs

samples:  Total number of fuzzy sets generated

filepath: Filepath to save the generated sets in csv format.
          First row lists x-coordinates
          Subsequent rows list associated membership values for each fuzzy set
"""


import sys
import numpy as np
import random
from decimal import Decimal


from utilities.discrete_t1_fuzzy_set import DiscreteT1FuzzySet
from utilities.gaussian import Gaussian
from utilities import generation_helper as genh
from utilities import general_helper as gelh
from utilities import file_helper as fileh
import user_defined_variables as udv


FLS_BASE_FUNCTIONS = [Gaussian(0, 0.1),
                      Gaussian(0.2, 0.1),
                      Gaussian(0.4, 0.1),
                      Gaussian(0.6, 0.1),
                      Gaussian(0.8, 0.1),
                      Gaussian(1, 0.1)]


def set_fls_base_functions():
    """Scale the fuzzy sets to udv.MEAN_RANGE."""
    # Recreate functions instead of adjusting scales to get correct max/min
    global FLS_BASE_FUNCTIONS
    FLS_BASE_FUNCTIONS = [Gaussian(f.mean * udv.MEAN_RANGE[1] - udv.MEAN_RANGE[0],
                                   f.std_dev * udv.MEAN_RANGE[1] - udv.MEAN_RANGE[0])
                          for f in FLS_BASE_FUNCTIONS]


def generate_sets_mode_1():
    """Return fuzzy sets with Gaussian functions."""
    sets = []
    locs, scales = genh.generate_locs_and_scales(udv.MEAN_RANGE, udv.VAR_RANGE,
                                                 udv.SAMPLES)
    weights = genh.generate_weights(udv.SAMPLES, udv.HEIGHT_RANGE_1)
    for sample_i in range(udv.SAMPLES):
        mf = Gaussian(locs[sample_i], scales[sample_i], weights[sample_i])
        points = dict((gelh.set_x(x), mf.calculate_membership(x))
                      for x in np.linspace(udv.MEAN_RANGE[0],
                                           udv.MEAN_RANGE[1],
                                           udv.DISC_X))
        sets.append(DiscreteT1FuzzySet(points))
    return sets


def generate_sets_mode_2():
    """Return fuzzy sets with Gaussian-based skewed and bimodal functions."""
    sets = []
    locs1, scales1 = genh.generate_locs_and_scales(udv.MEAN_RANGE,
                                                   udv.VAR_RANGE,
                                                   udv.SAMPLES)
    locs2, scales2 = genh.generate_locs_and_scales(udv.MEAN_RANGE,
                                                   udv.VAR_RANGE,
                                                   udv.SAMPLES)
    weights1 = genh.generate_weights(udv.SAMPLES, udv.HEIGHT_RANGE_1)
    weights2 = genh.generate_weights(udv.SAMPLES, udv.HEIGHT_RANGE_2)
    for sample_i in range(udv.SAMPLES):
        mf1 = Gaussian(locs1[sample_i], scales1[sample_i], weights1[sample_i])
        mf2 = Gaussian(locs2[sample_i], scales2[sample_i], weights2[sample_i])
        points = dict((gelh.set_x(x), max(mf1.calculate_membership(x),
                                          mf2.calculate_membership(x)))
                      for x in np.linspace(udv.MEAN_RANGE[0],
                                           udv.MEAN_RANGE[1],
                                           udv.DISC_X))
        sets.append(DiscreteT1FuzzySet(points))
    return sets


def generate_sets_mode_3():
    """Return fuzzy sets with functions as fuzzy logic system outputs."""
    sets = []
    for sample_i in range(udv.SAMPLES):
        joint_mfs = [FLS_BASE_FUNCTIONS[i]
                     for i in [random.randint(0, 4)
                               for j in range(udv.FLS_TOTAL_MFS_USED)]]
        alphas = [round(random.random(), 2)
                  for i in range(udv.FLS_TOTAL_MFS_USED)]
        # replicate max min composition
        points = dict((gelh.set_x(x),
                       max([min(alphas[i],
                                joint_mfs[i].calculate_membership(x))
                            for i in range(udv.FLS_TOTAL_MFS_USED)]))
                      for x in np.linspace(udv.MEAN_RANGE[0],
                                           udv.MEAN_RANGE[1],
                                           udv.DISC_X))
        sets.append(DiscreteT1FuzzySet(points))
    return sets


if __name__ == '__main__':
    try:
        mode, samples, filepath = sys.argv[1:]
    except ValueError:
        print 'Error: Incorrect arguments given.\n'
        print __doc__
    udv.SAMPLES = int(samples)
    if mode == '1':
        sets = generate_sets_mode_1()
    elif mode == '2':
        sets = generate_sets_mode_2()
    elif mode == '3':
        set_fls_base_functions()
        sets = generate_sets_mode_3()
    else:
        raise ValueError('Unrecognised mode of fuzzy set creation. ' +
                         'Choose 1, 2, or 3.')
    fileh.save_t1(sets, filepath, udv.MEAN_RANGE, udv.DISC_X)
