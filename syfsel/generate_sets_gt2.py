"""Generate fuzzy sets with different types of membership functions.

run:      python generate_sets.py mode samples filepath

example:  python generate_sets.py 1 5 sets.csv

mode:     Indicates the type of fuzzy sets generated
          1: Gaussian functions
          2: Gaussian-based skewed/bimodal functions
          3. Fuzzy logic system outputs

samples:  Total number of fuzzy sets generated

filepath: Filepath to save the generated sets in csv format.
          First row lists x-coordinates
          Subsequent rows list associated membership values for each fuzzy set
"""


import sys
import numpy as np
import random
from decimal import Decimal


from utilities.discrete_gt2_fuzzy_set import DiscreteGT2FuzzySet
from utilities.gaussian import Gaussian
from utilities import generation_helper as genh
from utilities import general_helper as gelh
from utilities import file_helper as fileh
import user_defined_variables as udv


FLS_BASE_FUNCTIONS = [genh.generate_zslices(Gaussian(0, 0.1, 0.8),
                                            Gaussian(0, 0.1)),
                      genh.generate_zslices(Gaussian(0.2, 0.1, 0.8),
                                            Gaussian(0.2, 0.1)),
                      genh.generate_zslices(Gaussian(0.4, 0.1, 0.8),
                                            Gaussian(0.4, 0.1)),
                      genh.generate_zslices(Gaussian(0.6, 0.1, 0.8),
                                            Gaussian(0.6, 0.1)),
                      genh.generate_zslices(Gaussian(0.8, 0.1, 0.8),
                                            Gaussian(0.8, 0.1)),
                      genh.generate_zslices(Gaussian(1, 0.1, 0.8),
                                            Gaussian(1, 0.1))]
SAMPLES = 1


def set_fls_base_functions():
    """Scale the fuzzy sets to udv.MEAN_RANGE."""
    # Recreate functions instead of adjusting scales to get correct max/min
    global FLS_BASE_FUNCTIONS
    FLS_BASE_FUNCTIONS = [genh.generate_zslices(
                           Gaussian(f[0.25][0].mean * udv.MEAN_RANGE[1] - udv.MEAN_RANGE[0],
                                    f[0.25][0].std_dev * udv.MEAN_RANGE[1] - udv.MEAN_RANGE[0],
                                    f[0.25][0].height),
                           Gaussian(f[0.25][1].mean * udv.MEAN_RANGE[1] - udv.MEAN_RANGE[0],
                                    f[0.25][1].std_dev * udv.MEAN_RANGE[1] - udv.MEAN_RANGE[0]),
                           udv.DISC_Z)
                          for f in FLS_BASE_FUNCTIONS]


def generate_sets_mode_1():
    """Return fuzzy sets with Gaussian functions."""
    sets = []
    locs, scales = genh.generate_locs_and_scales(udv.MEAN_RANGE, udv.VAR_RANGE,
                                                 SAMPLES)
    weights_umf = genh.generate_weights(SAMPLES, udv.HEIGHT_RANGE_1)
    weights_lmf = [i - Decimal(udv.HEIGHT_DECREASE_LMF) for i in weights_umf]
    zlevels = genh.calculate_zlevel_coords(udv.DISC_Z)
    for sample_i in range(SAMPLES):
        lmf = Gaussian(locs[sample_i], scales[sample_i], weights_lmf[sample_i])
        umf = Gaussian(locs[sample_i], scales[sample_i], weights_umf[sample_i])
        zslice_functions = genh.generate_zslices(lmf, umf, udv.DISC_Z)
        points = dict((gelh.set_x(x),
                       dict((z, (zslice_functions[z][0].
                                 calculate_membership(x),
                                 zslice_functions[z][1].
                                 calculate_membership(x)))
                       for z in zlevels))
                      for x in np.linspace(udv.MEAN_RANGE[0], udv.MEAN_RANGE[1], udv.DISC_X))
        sets.append(DiscreteGT2FuzzySet(points))
    return sets


def generate_sets_mode_2():
    """Return fuzzy sets with Gaussian-based skewed and bimodal functions."""
    sets = []
    locs1, scales1 = genh.generate_locs_and_scales(udv.MEAN_RANGE, udv.VAR_RANGE,
                                                   SAMPLES)
    locs2, scales2 = genh.generate_locs_and_scales(udv.MEAN_RANGE, udv.VAR_RANGE,
                                                   SAMPLES)
    weights1_umf = genh.generate_weights(SAMPLES, udv.HEIGHT_RANGE_1)
    weights2_umf = genh.generate_weights(SAMPLES ,udv.HEIGHT_RANGE_2)
    weights1_lmf = [i - Decimal(udv.HEIGHT_DECREASE_LMF) for i in weights1_umf]
    weights2_lmf = [i - Decimal(udv.HEIGHT_DECREASE_LMF) for i in weights2_umf]
    zlevels = genh.calculate_zlevel_coords(udv.DISC_Z)
    for sample_i in range(SAMPLES):
        lmf1 = Gaussian(locs1[sample_i], scales1[sample_i], weights1_lmf[sample_i])
        umf1 = Gaussian(locs1[sample_i], scales1[sample_i], weights1_umf[sample_i])
        lmf2 = Gaussian(locs2[sample_i], scales2[sample_i], weights2_lmf[sample_i])
        umf2 = Gaussian(locs2[sample_i], scales2[sample_i], weights2_umf[sample_i])
        zslice_functions1 = genh.generate_zslices(lmf1, umf1, udv.DISC_Z)
        zslice_functions2 = genh.generate_zslices(lmf2, umf2, udv.DISC_Z)
        points = dict((gelh.set_x(x),
                       dict((z, (max(zslice_functions1[z][0].
                                     calculate_membership(x),
                                     zslice_functions2[z][0].
                                     calculate_membership(x)),
                                 max(zslice_functions1[z][1].
                                     calculate_membership(x),
                                     zslice_functions2[z][1].
                                     calculate_membership(x))))
                            for z in zlevels))
                      for x in np.linspace(udv.MEAN_RANGE[0], udv.MEAN_RANGE[1], udv.DISC_X))
        sets.append(DiscreteGT2FuzzySet(points))
    return sets


def generate_sets_mode_3():
    """Return fuzzy sets with functions as fuzzy logic system outputs."""
    sets = []
    zlevels = genh.calculate_zlevel_coords(udv.DISC_Z)
    for sample_i in range(SAMPLES):
        joint_mfs = [FLS_BASE_FUNCTIONS[i]
                     for i in [random.randint(0, 4)
                               for j in range(udv.FLS_TOTAL_MFS_USED)]]
        alphas = [round(random.random(), 2) for i in range(udv.FLS_TOTAL_MFS_USED)]
        ualphas = [dict((z, max(0, Decimal(a)+((1-z)*Decimal('0.1'))))
                        for z in zlevels) for a in alphas]
        lalphas = [dict((z, max(0, Decimal(a)-((1-z)*Decimal('0.1'))))
                        for z in zlevels) for a in alphas]
        # replicate max min composition
        points = dict((gelh.set_x(x),
                       dict((z,
                             (max([min(lalphas[i][z],
                                       joint_mfs[i][z][0].
                                       calculate_membership(x))
                                   for i in range(udv.FLS_TOTAL_MFS_USED)]),
                              max([min(ualphas[i][z],
                                       joint_mfs[i][z][1].
                                       calculate_membership(x))
                                   for i in range(udv.FLS_TOTAL_MFS_USED)])))
                            for z in zlevels))
                      for x in np.linspace(udv.MEAN_RANGE[0], udv.MEAN_RANGE[1], udv.DISC_X))
        sets.append(DiscreteGT2FuzzySet(points))
    return sets


if __name__ == '__main__':
    try:
        mode, samples, filepath = sys.argv[1:]
    except ValueError:
        print 'Error: Incorrect arguments given.\n'
        print __doc__
    SAMPLES = int(samples)
    if mode == '1':
        sets = generate_sets_mode_1()
    elif mode == '2':
        sets = generate_sets_mode_2()
    elif mode == '3':
        set_fls_base_functions()
        sets = generate_sets_mode_3()
    else:
        raise ValueError('Unrecognised mode of fuzzy set creation. ' +
                         'Choose 1, 2, or 3.')
    fileh.save_gt2(sets, filepath, udv.MEAN_RANGE, udv.DISC_X, udv.DISC_Z)

