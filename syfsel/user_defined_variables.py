"""Use this module to set variables in generating fuzzy sets."""

DISC_X = 101  # total discretisations along x-axis
DISC_Z = 4  # total zlevels (secondary membership values) to define GT2
MEAN_RANGE = [0, 1]  # boundaries of x-axis (universe of discourse)
VAR_RANGE = [0.1, 0.25]
HEIGHT_RANGE_1 = [1, 1]  # height of L1 sets and of first L2 set
HEIGHT_RANGE_2 = [0.6, 0.8]  # height of second L2 set
HEIGHT_DECREASE_LMF = 0.3  # heights of lower membership functions
FLS_TOTAL_MFS_USED = 3  # total functions used to create FLS outputs
