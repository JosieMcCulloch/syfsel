SyFSeL (Synthetic Fuzzy Set Library)
===========

What is it?
-----------
Empirical tests can help determine if methods developed for fuzzy sets work correctly. However, finding a large enough data set with suitable properties to conduct thorough tests can be challenging. SyFSeL automatically generates synthetic fuzzy sets with specified characteristics and fuzzy set type. It generates as many sets as desired, with adjustable parameters to enable users to emulate real data. Fuzzy sets are stored in csv format so users can easily import the generated sets into their own fuzzy systems software. SyFSeL can also create graphical plots of the generated sets.


The Latest Version
------------------
The latest version can found at
https://bitbucket.org/JosieMcCulloch/syfsel


Prerequisites
--------------
* python 2.7
* decimal
* matplotlib (to use plot_sets_*.py)


Installation
------------
No installation is required.


Contacts
--------
If you wish to contact about new releases, feature requests or submit bugs,
please contact the mail address: josie.mcculloch@nottingham.ac.uk.


License
-------
Copyright (C) 2018 Josie McCulloch

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
